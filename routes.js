/**
 * @file Routes file
 * @author Peter Gates
 */

module.exports = function(app) {
  app.get('/', function(req, res) {
    res.send('Index Page');
  });

  let data = require('./controllers/data_controller');
  app.post('/import', data.addResults);
  app.get('/results/:testId/aggregate', data.aggregateResults);
};
