/**
 * @file Test framework for Markr server
 * @author Peter Gates
 */

/* ------------ DECLARATIONS ------------ */

let expect = require('chai').expect;
let server = require('../server.js');
let http = require('http');
let util = require('../utilities');
let request = require('request');

/* ------------ TESTS ------------ */
describe('Server', function() {
  before(function() {
    server.listen(8000);
  });

  after(function() {
    server.close();
  });

  indexTests();
  importTests();
  resultsTests();
});

/**
 * Tests the root '/' page (and base server functionality)
 */
function indexTests() {
  describe('/', function() {
    statusTest('/', 200);
    statusTest('/marks', 404);
    htmlContentsTest('/', 'Index Page');
  });
}

/**
 * Tests the /import page.
 * Tests data importing, parsing, duplicateResults, and error handling
 */
function importTests() {
  describe('/import', function() {
    this.timeout(15000);
    contentTypeTest('/import', 'other', 415);
    sendXMLTest('/import', './test/samples/sample_single-result.xml', 200);
    sendXMLTest('/import', './test/samples/sample_duplicate-data.xml', 200);
    sendXMLTest('/import', './test/samples/sample_missing-data.xml', 422);
    sendXMLTest('/import', './test/samples/sample_results.xml', 200);
    sendXMLTest('/import', './test/samples/sample_malformed.xml', 422);
  });
}

/**
 * Tests the /results/:test-id/aggregate page.
 * Tests data retrieval, aggregate calculation, and error handling
 */
function resultsTests() {
  describe('/results', function() {
    getResultsTest('9863', 200);
    getResultsTest('0000', 404);
  });
}

/* ------------ PRIVATE FUNCTIONS ------------ */

/**
 * Tests the response code for a given address
 * @param {*} address address path to be tested
 * @param {*} statusCode status code expected
 */
function statusTest(address, statusCode) {
  it('should return "' + statusCode + '" when "' + address + '" requested', function(done) {
    http.get('http://localhost:8000' + address, function(res) {
      expect(res.statusCode).to.be.equal(statusCode);
      done();
    });
  });
}

/**
 * Tests the contents of the response from a given address
 * @param {*} address address path to be tested
 * @param {*} contents page contents expected
 */
function htmlContentsTest(address, contents) {
  it('should say "' + contents + '"', function(done) {
    http.get('http://localhost:8000' + address, function(res) {
      let data = '';

      res.on('data', function(chunk) {
        data += chunk;
      });

      res.on('end', function() {
        expect(data).to.be.equal(contents);
        done();
      });
    });
  });
}

/**
 * Tests the response of the webpage to different POST content-types
 * @param {*} address address path to be tested
 * @param {*} contentType contentType to be tested
 * @param {*} expectedResponse expected response
 */
function contentTypeTest(address, contentType, expectedResponse) {
  it('should say "' + expectedResponse + '" for content-type ' + contentType, function(done) {
    let postOptions = {
      hostname: 'localhost',
      port: '8000',
      path: address,
      method: 'POST',
      headers: {
        'Content-Type': contentType,
      },
    };
    let message = JSON.stringify({
      'msg': 'Hello World!', // TODO: Replace with File Information
    });
    postData(postOptions, message, function(response) {
      expect(response.statusCode).to.be.equal(expectedResponse);
      done();
    });
  });
}

/**
 * Tests the response of the server to different types of XML samples
 * @param {*} address address path to be tested
 * @param {*} xmlFilePath relative path of the XML file to be submitted
 * @param {*} expectedResponse expected response from server
 */
function sendXMLTest(address, xmlFilePath, expectedResponse) {
  it('should say "' + expectedResponse + '" for XML Sample ' + xmlFilePath, function(done) {
    let data = util.loadXML(xmlFilePath);
    let options = {
      url: 'http://localhost:8000' + address,
      method: 'POST',
      keepAlive: false,
      headers: {
        'Content-Type': 'text/xml+markr',
        'Accept-Encoding': 'UTF-8',
      },
      body: data,
    };

    request.post(options, function(error, response, body) {
      expect(response.statusCode).to.be.equal(expectedResponse);
      done();
    });
  });
}

/**
 * Tests the results of the server for a given test-id
 * @param {*} testId Id of the requested test
 * @param {*} expectedResponse expected response from server
 */
function getResultsTest(testId, expectedResponse) {
  it('should say "' + expectedResponse + '" for test-id ' + testId, function(done) {
    let address = '/results/' + testId + '/aggregate';

    http.get('http://localhost:8000' + address, function(response) {
      let data = '';

      response.on('data', function(chunk) {
        data += chunk;
      });

      response.on('end', function() {
        expect(response.statusCode).to.be.equal(expectedResponse);
        done();
        console.log('        ---> Result: ' + data);
      });
    });
  });
}

/* ------------ HELPER FUNCTIONS ------------ */

/**
 * Posts data to a given endpoint and returns the response
 * @param {*} options HTTP Post options
 * @param {string} message message to be posted to server
 * @param {*} callback callback function containing response information
 */
function postData(options, message, callback) {
  let responseStatus;
  const request = http.request(options, (response) => {
    callback(response);
  });
  request.on('error', (error) => {
    console.error('Problem with Request: ' + error.message);
  });
  request.write(message);
  request.end();
}

// xmlParser.loadXML('./test/sample_results.xml');
