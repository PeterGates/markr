let fs = require('fs');
let xml2js = require('xml2js');
let parser = new xml2js.Parser();

/**
 * Loads an XML file and returns a string
 * @param {string} fileLocation relative location of the XML file to be parsed
 * @returns {string} string of xml file
 */
exports.loadXML = function(fileLocation) {
  let parsedResult = '';
  let data = fs.readFileSync(fileLocation, 'utf8');
  return data;
};

/**
 * Parses XML object into JS Object
 * @param {*} data XML data to be parsed
 * @returns {object} JS object of parsed XML
 */
exports.parseXML = function(data) {
  let parsedResult;
  parser.parseString(data, function(parseErr, result) {
    if (parseErr === null) {
      parsedResult = result;
    } else {
      // console.error(parseErr);
    }
  });
  return parsedResult;
};

/**
 * calculates a given percentile for an array of numbers
 * @param {*} numbers array of numbers
 * @param {float} percentile percentile wanted as a percentage (i.e. 50% = 0.5)
 * @return {float} calculate percentile
 */
exports.calculatePercentile = function(numbers, percentile) {
  // Sort Numbers
  numbers.sort(function(a, b) {
    return a - b;
  });

  let index = numbers.length * percentile;
  if (index === parseInt(index, 10)) {
    result = (numbers[index] + numbers[index + 1]) / 2;
  } else {
    result = numbers[Math.round(index)];
  }
  return parseFloat(result);
};
