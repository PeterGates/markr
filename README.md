# Markr

Environment
===========
Compatible/tested environment(s)

* Windows 10 (Version 1709), node v8.9.1, npm 5.5.1
* Ubuntu 16.04.3 LTS, node v8.9.1, npm 5.5.1

Setup
=====
To install the application, ensure that you have the required versions of node and npm, then `cd` to the root directory and invoke:
```
npm install
```
This will automatically download the required node modules.

Usage
=====
Run:
-----
To run the application, invoke:
```
npm start
```
This will start the server on localhost:3001.

Test:
-----
To run the test suite, cd into the root directory and run the following commmand:
```
npm test
```
This will run a series of tests using the test samples (./test/samples) on localhost:8000.

Notes
=====
Assumptions:
------------
In developing this application, a number of assumptions were made:

* Leading 0s are important for the test-id, and thus can't be truncated.
* The important parts of each results entry was student-number, test-id, and summary-marks (both marks-available and marks-awarded). Without these being present and correctly parsed, the results can't be inserted into the database and aggregates calculated correctly.
* Appropriate error code for missing 'important bits' is 422 (Unprocessable Entity). 
* The server should return 415 (Unsupported Media Type) for an incorrect 'content-type' header.


Approach:
------------
The development of this application was approached in the following manner:

1. The specification was exaimined, and the import aspects of it broken into specific tasks (e.g. `/import` and `/results/:test-id/aggregate` endpoints)
2. Application architecture was considered, as well as what frameworks/libraries and database would be used
3. Each task (from 1) was then further broken down into the functions that would be required
4. These functions formed the initial scaffold for the project
5. The test harness was then constructed, allowing for the tests to be developed alongside each function (to allow for rapid testing)
6. For each endpoint, each of the functions were then systematically developed and tested, including documentation


Other Notes:
------------

* nedb was selected for the database for two reasons:

  1. It was a lightwight database implementation that didn't require a seperate server running to access the data. This also made it extremely portable.
  2. It uses a subset of the Mongodb API calls, and thus when the product is scaled for real-time dashboards, the system can be upgraded to use MongoDB as the database with only minor changes.

* Real time dashboards could be implemented by upgrading the database to MongoDB, as mentioned above, and setting up a socket connection between the back-end and front-end clients. This means that the front-end clients could simply listen out for updates broadcast by the back-end, rather than constantly polling the system (which is ineffecient)

Lisence
=======
See [LICENSE](./LICENSE)
