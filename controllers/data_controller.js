/**
 * @file Controllor for data importing, exporting, and management
 * @author Peter Gates
 */

/* ------------ DECLARATIONS ------------ */

let Datastore = require('nedb');
let db = new Datastore({
  filename: './database/results.db',
  autoload: true,
});
// Index Database at startup to improve search speed
db.ensureIndex({fieldName: 'test_id'}, function(err) {
  // If there was an error, err is not null
});
let util = require('../utilities');


/* ------------ PUBLIC FUNCTIONS ------------ */

/**
 * Post /import. Takes HTTP Post request, checks if it's valid, and then inputs it into the Database if correct
 * @param {*} req HTTP POST
 * @param {*} res Response.
 */
exports.addResults = function(req, res) {
  let status = {
    statusCode: 0,
    message: '',
  };
  // Check if content-type is correct
  if (req.get('content-type') !== 'text/xml+markr') {
    // If not, return error 415
    status.statusCode = 415;
    status.message = 'Incorrect Content-Type';
    console.log('        Incorrect Content-Type --->');
  } else {
    // If so, read results from body
    status = readResults(req.body);
  }
  res.status(status.statusCode).send(status.message);
};

/**
 * Get /results/<TEST_ID>/aggregate. Takes an HTTP request and returns the results based on testId
 * @param {*} req HTTP Request, contains testId
 * @param {*} res Response. Returns a JSON document with mean, count, p25, p50, and p75 (as per specification)
 */
exports.aggregateResults = function(req, res) {
  let testId = req.params.testId;
  retrieveResults(testId, function(results) {
    // console.log(results);
    if (results) {
      res.send(JSON.stringify(results));
    } else {
      res.status(404).send('No Data Found');
    }
  });
};

/* ------------ PRIVATE FUNCTIONS ------------ */

/**
 * Takes XML and determines if it is valid. If so, it removes duplicates and then inserts it into the database
 * @param {*} resultsBody body content from POST request 
 * @returns {object} returns object with status code and message (to respond to HTTP request)
 */
function readResults(resultsBody) {
  let validImport = false;
  let status = {
    statusCode: 0,
    message: '',
  };
  let data = '';
  // Try to parse receieved XML
  let validityResult = checkValidity(resultsBody);
  let parsedData = validityResult.results;

  if (parsedData === null) {
    // If Results can't be passed, return error 422
    status.statusCode = validityResult.status.statusCode;
    status.message = validityResult.status.message;
  } else {
    // If Results can be parsed, check for duplicates
    status.statusCode = 200;
    status.message = 'OK';
    let finalData = checkDuplicates(parsedData);
    // Insert data (with duplicates removed) into the database
    finalData.forEach((data) => {
      insertResult(data);
    });
  }

  return status;
}

/**
 * Checks the validity of the XML Body and for missing critical information
 * @param {*} resultsBody XML Body from POST Request
 * @returns {object} if valid, returns the parsed XML as an object, otherwise it returns an error code
 */
function checkValidity(resultsBody) {
  let result = {
    status: {
      statusCode: 0,
      message: '',
    },
    results: null,
  };
  try {
    let body = util.parseXML(resultsBody);
    data = body['mcq-test-results']['mcq-test-result'];
    parsedData = parseResults(data);
    result.results = parsedData;
    if (parsedData === null) {
      // If Results are missing critical data, return 422
      result.status.statusCode = 422;
      result.status.message = 'Invalid Results. Missing Critical Data.';
      console.log('        Invalid Results. Missing Critical Data --->');
    }
  } catch (err) {
    // If error in parseXML function, return error 422 (with error)
    result.status.statusCode = 422;
    result.status.message = 'Invalid Results. Malformed Result: ' + err;
    console.log('        Invalid Results. Malformed XML Content --->');
  }
  return result;
}

/**
 * Takes results array and checks each result is valid and is not missing critical data
 * @param {object} results results array
 * @returns {object} array of parsed results
 */
function parseResults(results) {
  let data = [];
  for (let resultIdx = 0; resultIdx < results.length; resultIdx++) {
    const result = results[resultIdx];
    if (!checkResults(result)) {
      data = null;
      break;
    }
    data.push(result);
  }
  return data;
}

/**
 * Check the given results input to determine if it is valid
 * @param {*} result results input from HTTP POST
 * @returns {object} validity of results
 */
function checkResults(result) {
  // Check for valid test-id, student-number and summary mark values
  let testIDValid;
  let summaryMarksValid;
  let availableMarksValid;
  let obtainedMarksValid;

  testIDValid = Boolean(result['test-id'][0]);
  studentNumberValid = Boolean(result['student-number'][0]);
  summaryMarksValid = Boolean(result['summary-marks']);
  if (summaryMarksValid) {
    availableMarksValid = Boolean(result['summary-marks'][0]['$']['available']) && !isNaN(result['summary-marks'][0]['$']['available']);
    obtainedMarksValid = Boolean(result['summary-marks'][0]['$']['obtained']) && !isNaN(result['summary-marks'][0]['$']['obtained']);
  }
  let valid = testIDValid && studentNumberValid && availableMarksValid && obtainedMarksValid;
  return valid;
}

/**
 * iterates over the results array to find any duplicates
 * @param {*} results array of results
 * @returns {array} finalised results array
 */
function checkDuplicates(results) {
  let finalResults = [];
  // Iterate through the list of results and compares them to the growing list of 'final results'
  results.forEach((result) => {
    let finalResult = result;
    for (let existingResultIdx = 0; existingResultIdx < finalResults.length; existingResultIdx++) {
      let existingResult = finalResults[existingResultIdx];
      comparisonResult = compareResults(result, existingResult);
      if (comparisonResult.duplicateFound) {
        // Remove Existing Duplicate from Results List. Replace with New Result
        finalResults.splice(existingResultIdx, 1);
        finalResult = comparisonResult.finalResult;
      }
      // }
    }
    finalResults.push(finalResult);
  });
  return finalResults;
}

/**
 * compares two results and, if they are the same, takes the higher of the two
 * @param {object} firstResult first result to be compared
 * @param {object} secondResult second result to be compared
 * @returns {object} highest result
 */
function compareResults(firstResult, secondResult) {
  let finalResult = firstResult;
  let firstResultValues = firstResult['summary-marks'][0]['$'];
  let secondResultValues = secondResult['summary-marks'][0]['$'];
  let result = {
    finalResult: firstResult,
    duplicateFound: false,
  };

  if (firstResult['student-number'][0] === secondResult['student-number'][0] && firstResult['test-id'][0] === secondResult['test-id'][0]) {
    // Duplicate Found! Get Highest Values
    result.duplicateFound = true;
    result.finalResult['summary-marks'][0]['$']['available'] = Math.max(parseInt(firstResultValues['available']), parseInt(secondResultValues['available']));
    result.finalResult['summary-marks'][0]['$']['obtained'] = Math.max(parseInt(firstResultValues['obtained']), parseInt(secondResultValues['obtained']));
  }
  return result;
}

/**
 * Inserts result into the database
 * @param {*} result test result to be inserted into the database
 */
function insertResult(result) {
  // nedb doesn't allow for schema creation, so we will set the document structure here
  let resultDoc = {
    _id: result['test-id'][0] + '_' + result['student-number'][0],
    scanned: result['$']['scanned-on'],
    first_name: result['first-name'][0],
    last_name: result['last-name'][0],
    student_number: result['student-number'][0],
    test_id: result['test-id'][0],
    summary_marks: {
      available: result['summary-marks'][0]['$']['available'],
      obtained: result['summary-marks'][0]['$']['obtained'],
    },
  };
  db.insert(resultDoc, function(err) {
    // Inserts document into the database
  });
}

/**
 * retrieves all the results for a given testId
 * @param {string} testId testId of requested test data
 * @param {object} callback returns results for test
 */
function retrieveResults(testId, callback) {
  let results;
  // console.log('Test-ID: ' + testId);
  db.find({test_id: testId}, function(err, document) {
    if (document.length > 0) {
      results = calculateAggregate(document);
      // console.log('Results: ' + JSON.stringify(results));
    } else {
      results = null;
    }
    callback(results);
  });
}

/**
 * calculates aggregate values based on results input
 * @param {object} results array of results to be used for calculation
 * @returns {object} aggregate result
 */
function calculateAggregate(results) {
  let marks = [];
  let availableMarks = 0;
  let total = 0;
  results.forEach(function(result) {
    let mark = parseInt(result.summary_marks.obtained);
    availableMarks = Math.max(parseInt(result.summary_marks.available), availableMarks);
    marks.push(mark);
    total = total + mark;
  });

  // Note: the visualisation team require these numbers to be expressed as percentages (i.e. as a float from 0 to 100) of the available marks in the test.
  let count = marks.length;
  let mean = total / count / availableMarks * 100;
  let p25 = util.calculatePercentile(marks, 0.25) * 100 / availableMarks;
  let p50 = util.calculatePercentile(marks, 0.50) * 100 / availableMarks;
  let p75 = util.calculatePercentile(marks, 0.75) * 100 / availableMarks;


  let resultsAggregate = {
    mean: mean,
    count: marks.length,
    p25: p25,
    p50: p50,
    p75: p75,
  };
  return resultsAggregate;
}
