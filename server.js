/**
 * @file Main application for Markr server coding challange
 * @author Peter Gates
 */

/* ------------ DECLARATIONS ------------ */

// Load Modules
let express = require('express');
let app = express();
let bodyParser = require('body-parser');

app.use(bodyParser.text({type: 'text/xml+markr', limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

// Routes
require('./routes')(app);

/* ------------ PUBLIC FUNCTIONS ------------ */

exports.listen = function(port) {
  let server = app.listen(port);
  console.log('    Listening on Port ' + port);

  // Close server when close() is called
  exports.close = function(callback) {
    server.close();
    console.log('    Server Shutdown');
  };
};

/* ------------ MAIN APP CODE ------------ */

// Check if server.js is being used as a module, or called directly
if (require.main === module) {
  // Start Server on Port 3001
  this.listen(3001);
} else {
  // Do Nothing
}
